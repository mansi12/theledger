import React, { useEffect, useState } from "react";
import ViewShipment from "../../components/viewShipment";
import Header from "../../shared/header";
import Sidebar from "../../shared/sidebarMenu";
import {
  getViewShipment,
  fetchIotEnabledApiResponse,
} from "../../actions/shipmentActions";
import { useDispatch } from "react-redux";
import { chainOfCustody, fetchImage } from "../../actions/shipmentActions";
import { useIotShipmentData } from "../../hooks/useIotShipmentData";
import { config } from "../../config";
import { useTranslation } from 'react-i18next';

const ViewGMRShipmentContainer = (props) => {
const { t, i18n } = useTranslation();
  const [trackData, setTrackData] = useState({});
  const [shippmentChainOfCustodyData, setShippmentChainOfCustodyData] =
    useState([]);
  const [imagesData, setImagesData] = useState([]);
  const [iotEnabledStatus, setIotEnabledStatus] = useState(false);
  const [imageData, setImageData] = useState([]);

  const dispatch = useDispatch();

  useEffect(() => {
    async function fetchData() {
      const result = await dispatch(getViewShipment(props.match.params.id));
      if (result) {
        setTrackData(result);
      } else {
        setTrackData({});
      }
    }
    fetchData();
  }, [dispatch, props.match.params.id]);

  useEffect(() => {
    async function fetchData() {
      const result = await chainOfCustody(props.match.params.id);
      if (result && result?.status === 200) {
        setShippmentChainOfCustodyData(
          result.data.data["shipmentChainOfCustody"]
        );
      } else {
        setShippmentChainOfCustodyData([]);
      }
    }
    fetchData();
  }, [props.match.params.id]);

  useEffect(() => {
    async function fetchData() {
      const result = await fetchImage(props.match.params.id);
      if (result?.status === 200) {
        setImagesData(result.data.data);
      } else {
        setImageData([]);
      }
    }
    fetchData();
  }, [props.match.params.id]);

  /**
   * Enable temperature graph based on IotEnabledStatus
   */
  useEffect(() => {
    async function fetchIotEnabledStatus() {
      const result = await fetchIotEnabledApiResponse(props.match.params.id);

      if (result.data.iot_enabled) {
        setIotEnabledStatus(true);
      } else {
        setIotEnabledStatus(false);
      }
    }
    fetchIotEnabledStatus();
  }, [props.match.params.id]);

  // const latestIotShipmentData = useIotShipmentData(
  //   config().trackLatestShipmentData.replace(
  //     ":shipmentId",
  //     props.match.params.id
  //   ),
  //   iotEnabledStatus
  // );
  // const lastTenIotShipmentData = useIotShipmentData(
  //   config().trackLastTenIotShipmentData.replace(
  //     ":shipmentId",
  //     props.match.params.id
  //   ),
  //   iotEnabledStatus
  // );

  const openInTrackingPage = () => {
    props.history.push(`/tracing/${props.match.params.id}?status=shipmentView`);
  };

  return (
    <div className='container-fluid p-0'>
      <Header {...props} t={t}/>
      <div className='d-flex'>
        <Sidebar {...props} t={t}/>
        <div className='content'>
          <ViewShipment
            trackData={trackData}
            shippmentChainOfCustodyData={shippmentChainOfCustodyData}
            imagesData={imagesData}
            // latestIotShipmentData={latestIotShipmentData}
            // lastTenIotShipmentData={lastTenIotShipmentData}
            iotEnabledStatus={iotEnabledStatus}
            openInTrackingPage={openInTrackingPage}
            t={t}
            {...props}
          />
        </div>
      </div>
    </div>
  );
};
export default ViewGMRShipmentContainer;
